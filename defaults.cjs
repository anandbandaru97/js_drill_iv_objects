// function defaults(obj, defaultProps) {
//     // Fill in undefined properties that match properties on the `defaultProps` parameter object.
//     // Return `obj`.
//     // http://underscorejs.org/#defaults
// }

const defaults = (obj, defaultProps) => {
    if (typeof (obj) !== 'object' || Array.isArray(obj)) {
        return 'Invalid Object';
    }
    if (typeof (defaultProps) !== 'object' || Array.isArray(obj)) {
        return 'Invalid Object';
    }
    for (let key in defaultProps) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
};

module.exports = defaults;