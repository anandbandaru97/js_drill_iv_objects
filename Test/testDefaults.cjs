const testObject = { name: 'Bruce Wayne', location: 'Gotham' };

const defaults = require('../defaults.cjs');

console.log(defaults(testObject, { name: 'Bruce Wayne', age: 25, location: 'Gotham' }));