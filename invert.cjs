// function invert(obj) {
//     // Returns a copy of the object where the keys have become the values and the values the keys.
//     // Assume that all of the object's values will be unique and string serializable.
//     // http://underscorejs.org/#invert
// }

const invert = (obj) => {
    if (typeof (obj) !== 'object' || Array.isArray(obj)) {
        return 'Invalid Object';
    }
    for (let key in obj) {
        if (typeof (obj[key] === 'function' || typeof (obj[key]) === 'undefined')) {
            obj[obj[key]] = key;
        } 
        if (obj[key] !== undefined) {
            delete obj[key];
        }
    }
    return obj;
};

module.exports = invert;