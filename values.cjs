// function values(obj) {
//     // Return all of the values of the object's own properties.
//     // Ignore functions
//     // http://underscorejs.org/#values
// }

const values = (obj) => {
    if (typeof(obj) !== 'object' || Array.isArray(obj)){
        return 'Invalid Object';
    }
    let valuesArray = [];
    for (let key in obj) {
        valuesArray.push(obj[key]);
    }
    return valuesArray;
};

module.exports = values;