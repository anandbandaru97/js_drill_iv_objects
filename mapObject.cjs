// function mapObject(obj, cb) {
//     // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
//     // http://underscorejs.org/#mapObject
// }

const mapObject = (obj, cb) => {
    if (typeof (obj) !== 'object' || Array.isArray(obj)) {
        return 'Invalid Object';
    }
    for (let key in obj) {
        obj[key] = cb(obj[key]);
    }
    return obj;
};

module.exports = mapObject;