// function pairs(obj) {
//     // Convert an object into a list of [key, value] pairs.
//     // http://underscorejs.org/#pairs
// }

const pairs = (obj) => {
    if (typeof(obj) !== 'object' || Array.isArray(obj)){
        return 'Invalid Object';
    }
    let pairsArray = [];
    for (let key in obj) {
        pairsArray.push([key, obj[key]]);
    }
    return pairsArray;
};

module.exports = pairs;